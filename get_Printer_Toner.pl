#get_Printer_Toner.pl
#created by _KUL (Kuleshov)
#version 2018-11-15
#Apache License 2.0

#!/usr/bin/perl
#use warnings;
use strict;
use v5.14;
use Net::SNMP;

my %toner = (
				'black' =>
			 				{
								'tk-580k' => '1',
								'TK-895K' => '1',
								'black toner' => '1',
								'black cartridge hp' => '1',
								'black high print cartridge' => '1',
								'black print cartridge' => '1',
								'Black Cartridge HP CC530A' => '1',
								'Black Standard Print Cartridge' => '1',
								'd187d0b5d180d0bdd18bd0bc20d182d0bed0bdd0b5d180d0bed0bc' => '1',
							},
				'yellow' => {
								'tk-580y' => '1',
								'TK-895Y' => '1',
								'yellow cartridge hp' => '1',
								'yellow print cartridge' => '1',
								'Yellow Cartridge HP CC532A' => '1',
								'd0b6d0b5d0bbd182d18bd0bc20d182d0bed0bdd0b5d180d0bed0bc' => '1',
							},
				'magenta' => {
								'tk-580m' => '1',
								'TK-895M' => '1',
								'magenta cartridge hp' => '1',
								'magenta print cartridge' => '1',
								'Magenta Cartridge HP CC533A' => '1',
								'd0bcd0b0d0bbd0b8d0bdd0bed0b2d18bd0bc20d182d0bed0bdd0b5d180d0bed0bc' => '1',
							},
				'cyan' => {
								'tk-580c' => '1',
								'TK-895C' => '1',
								'cyan cartridge hp' => '1',
								'cyan print cartridge' => '1',
								'Cyan Cartridge HP CC531A' => '1',
								'd0b3d0bed0bbd183d0b1d18bd0bc20d182d0bed0bdd0b5d180d0bed0bc' => '1',
						},

				'black-drum' => {
								'black drum cartridge' => '1',
						},
);

if (defined($ARGV[0]) and defined($ARGV[1]) and defined($ARGV[2]) and defined($ARGV[3])) {
my ($session) = Net::SNMP->session(
                                -timeout => 2,
                                -retries => 2,
                                -hostname => "$ARGV[0]",
                                -community => "$ARGV[1]",
                            ) or die $!;
		
	my $table = $session->get_table(-baseoid =>"1.3.6.1.2.1.43.11.1.1");
	my ($oidToner, $tmp);
	foreach my $elem (keys %{$table}) {
		$tmp = lc $table->{$elem};
		foreach my $find (keys %{$toner{$ARGV[2]}}) {
			$find = lc $find; 
			if ($tmp =~ m/$find/) {
				$oidToner = $elem;
				last;
			}
		}
		last if (defined($oidToner));
	}
	if (defined($oidToner)) {
		my @tmp = split /\./,$oidToner;
		if ($ARGV[3] eq 'percent') {
			if (($table->{"1.3.6.1.2.1.43.11.1.1.9.$tmp[11].$tmp[12]"} ge 0) and ($table->{"1.3.6.1.2.1.43.11.1.1.8.$tmp[11].$tmp[12]"} ge 0)) {
				my $tosay = (($table->{"1.3.6.1.2.1.43.11.1.1.9.$tmp[11].$tmp[12]"}/$table->{"1.3.6.1.2.1.43.11.1.1.8.$tmp[11].$tmp[12]"})*100);
				($tosay < 101) ? say sprintf("%.0f",$tosay): say '0';
			}
		}
		if ($ARGV[3] eq 'count') {
			say $table->{"1.3.6.1.2.1.43.11.1.1.9.$tmp[11].$tmp[12]"} if ($table->{"1.3.6.1.2.1.43.11.1.1.9.$tmp[11].$tmp[12]"} ge 0);
		}
	}
}
